package com.example.aishu.login;

import android.view.View;
import android.widget.ListView;

public class DataModel {


    String pid;
   String iname;
    String price;
    String imgurl;
    String cat;


    public String getPid() {
        return pid;
    }
    public String getIname() {
        return iname;
    }

    public String getPrice() {
        return price;
    }

    public String getImgurl() {
        return imgurl;
    }

    public String getCat() {
        return cat;
    }



   public DataModel(String pid, String iname, String price, String imgurl, String cat)
   {
       this.pid=pid;
       this.iname=iname;
       this.price=price;
       this.imgurl=imgurl;
       this.cat=cat;
   }
}
