package com.example.aishu.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class ScanActivity extends AppCompatActivity{
    FirebaseAuth mAuth;
    ArrayList<DataModel> datamodel;
    ListView listView;
    CustomAdapter adapter;
    Context context;
    DataModel indexdel;
    String tid;
    String user_id;
    Button btnsubmit;
    Dialog dialog;
    TextView iname,icat,idesc,ibrand,iprice;
    EditText coupon;
    ImageButton close;
    ImageView imgurl;
    KProgressHUD hud;
    String cc;
//    int prod_p;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private NavigationView navigationView;
    TextView txtprice;
    private ImageButton btnscan;
    private TextView barcode;
    String value;
    private IntentIntegrator qrScan;
    float historicX = Float.NaN, historicY = Float.NaN;
    static final int DELTA = 50;
    enum Direction {LEFT, RIGHT;}
    TextToSpeech t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        setTitle("Cart");

        navigationView=(NavigationView)findViewById(R.id.nav_view);
        View navView=navigationView.inflateHeaderView(R.layout.nav_header);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context=getApplicationContext();
        txtprice = (TextView) findViewById(R.id.txt_price);
        txtprice.setText("0");
        mAuth = FirebaseAuth.getInstance();
        btnscan =  (ImageButton)findViewById(R.id.btnscan);
       // barcode = (TextView) findViewById(R.id.barcode);
        qrScan = new IntentIntegrator(this);
        btnscan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();
            }
        });
        btnsubmit=(Button) findViewById(R.id.btnsubmit);
        listView=(ListView)findViewById(R.id.list);
        coupon=(EditText)findViewById(R.id.coupon);

        datamodel= new ArrayList<>();
       // datamodel.add(new DataModel("Dress", "500", "1","women"));
      //  datamodel.add(new DataModel("Shirt", "1000", "2","men"));
        adapter= new CustomAdapter(datamodel,getApplicationContext());

        dialog= new Dialog(ScanActivity.this);

        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                indexdel=datamodel.get(position);
                tid=indexdel.getPid();
             //   Toast.makeText(context,tid ,Toast.LENGTH_SHORT).show();
             //   prod_p=Integer.getInteger(indexdel.getPrice());
                DeleteT obj1=new DeleteT(tid,indexdel,context,datamodel,listView,adapter, txtprice);
                obj1.execute();

//                indexdel=datamodel.get(position);
//                tid=indexdel.getPid();
//               // Toast.makeText(context,tid ,Toast.LENGTH_SHORT).show();
//                Desc objd= new Desc(tid,ScanActivity.this,dialog);
//                objd.execute();
                return false;
            }
        });

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                indexdel=datamodel.get(position);
//                tid=indexdel.getPid();
//                //Toast.makeText(getApplicationContext(),tid +"on clicking",Toast.LENGTH_SHORT).show();
//                Desc objd= new Desc(tid,context);
//                objd.execute();
                indexdel=datamodel.get(position);
                tid=indexdel.getPid();
                // Toast.makeText(context,tid ,Toast.LENGTH_SHORT).show();
                Desc objd= new Desc(tid,ScanActivity.this,dialog);
                objd.execute();
            }
        });



        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // btnsubmit.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                cc=coupon.getText().toString();
                coupon.setText(" ");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(coupon.getWindowToken(), 0);
                if(cc.equals(""))
                {
                    cc="0";
                }
                Total obj2=new Total(user_id,context,datamodel,listView,adapter,txtprice,cc);
                obj2.execute();

            }
        });



        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                displaySelectedScreen(id);
                return false;
            }
        });




      //  dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
      //  dialog.setCancelable(false);
        dialog.setContentView(R.layout.activity_desc);

         iname= (TextView) dialog.findViewById(R.id.iname);
         iprice= (TextView) dialog.findViewById(R.id.iprice);
         imgurl= (ImageView) dialog.findViewById(R.id.img);
         icat= (TextView) dialog.findViewById(R.id.icat);
         ibrand= (TextView) dialog.findViewById(R.id.ibrand);
         idesc= (TextView) dialog.findViewById(R.id.desc);
         close=(ImageButton) dialog.findViewById(R.id.close);
     //    dialog.show();

        hud =KProgressHUD.create(ScanActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);




    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(mToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int id) {
        switch (id) {
            case R.id.nav_cart:
                SendUsertoScanActivity();
                break;
            case R.id.nav_ar:
                sendUsertoArActivity();
                break;
            case R.id.nav_hist:
                SendUsertoHistActivity();
                break;
            case R.id.nav_chatbot:
                SendUsertoChatbotActivity();
                break;
            case R.id.nav_profile:
                SendUsertoProfileActivity();
                break;
            case R.id.nav_signout:
                SendUsertoMainActivity();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Intent i = new Intent(this, ChatActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }
    private void SendUsertoScanActivity() {
        Intent findIntent=new Intent(ScanActivity.this,ScanActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }

    private void SendUsertoHistActivity() {
        Intent findIntent=new Intent(ScanActivity.this,Hist.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }

    private void SendUsertoChatbotActivity() {
        Intent findIntent=new Intent(ScanActivity.this,ChatActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }
    private void SendUsertoProfileActivity() {
        Intent findIntent=new Intent(ScanActivity.this,RegisterActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }
    private void SendUsertoMainActivity() {
        FirebaseAuth.getInstance().signOut();
        Intent findIntent=new Intent(ScanActivity.this,MainActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }
    private void sendUsertoArActivity() {
        Intent findIntent = getPackageManager().getLaunchIntentForPackage("com.Ansh.SmartCart");
        if (findIntent != null) {
            startActivity(findIntent);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        user_id=firebaseUser.getEmail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                    value= result.getContents();
                  //  barcode.setText(value);
                   // Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
        hud.show();
        Product_Ret obj= new Product_Ret(user_id,value,context,datamodel,listView,adapter,txtprice,hud);
        obj.execute();

    }



    public void showDialog( String prod_id, String iname1, String iprice1, String imgurl1, String icat1, String ibrand1, String idesc1)
    {
       // final Dialog dialog=new Dialog(getApplicationContext());


   //     Toast.makeText(getApplicationContext(), idesc1,Toast.LENGTH_SHORT).show();
        t1.speak(idesc1, TextToSpeech.QUEUE_FLUSH, null);
        iname.setText(iname1);
        iprice.setText(iprice1);
        icat.setText(icat1);
        ibrand.setText(ibrand1);
        idesc.setText(idesc1);
        Picasso.with(context).load(imgurl1).into(imgurl);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

     dialog.show();
    }

}
