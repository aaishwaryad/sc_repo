package com.example.aishu.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;


public class RegisterDB extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/insertProfile.php";
    String fname="", lname="", phone="", age1="",balance="", gender_value="", email=" ";
     Context context;


    public RegisterDB(String fname,String lname,String phone,String age1,String balance, String gender_value, String email, Context context)
    {
        this.fname=fname;
        this.lname=lname;
        this.phone=phone;
        this.age1=age1;
        this.balance=balance;
        this.gender_value=gender_value;
        this.email=email;
        this.context=context;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if (response.contains("success")) {
                        Toast.makeText(context, "Profile Updated", Toast.LENGTH_SHORT).show();
                        RegisterActivity.flag=0;
                        sendUserToChatActivity();
                    }
                    else Toast.makeText(context, "failed" +response, Toast.LENGTH_SHORT).show();
                }
                catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                }


                }

        },
        new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("fname",fname);
                params.put("lname",lname);
                params.put("phone",phone);
                params.put("age",age1);
                params.put("balance",balance);
                params.put("gender",gender_value);
                params.put("email",email);

                return params;
            }
        };
        mRequestQueue.add(postrequest);

        return null;
    }

    private void sendUserToChatActivity() {
        Intent mainIntent=new Intent(context,ChatActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(mainIntent);
        //((Activity)context).finish();
       // context.do
    }

}

