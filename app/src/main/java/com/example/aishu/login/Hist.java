package com.example.aishu.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Hist extends AppCompatActivity{
    LottieAnimationView lottieAnimationView;
    FirebaseAuth mAuth;
    ArrayList<DataModel2> datamodel2;
    ListView listView;
    CustomAdapter2 adapter;
    Context context;
  //  DataModel2 dataModel2;
    String user_id;
   // KProgressHUD hud;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        setTitle("History");

        context=getApplicationContext();
        mAuth = FirebaseAuth.getInstance();
        listView=(ListView)findViewById(R.id.lhist);
        datamodel2= new ArrayList<>();
        adapter= new CustomAdapter2(datamodel2,getApplicationContext());
        listView.setAdapter(adapter);



        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        user_id=firebaseUser.getEmail();

      //  Toast.makeText(context,user_id,Toast.LENGTH_SHORT).show();
//        KProgressHUD hud =KProgressHUD.create(this)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDetailsLabel("Downloading data")
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setDimAmount(0.5f).show();

        lottieAnimationView = (LottieAnimationView) findViewById(R.id.animation_view);
        lottieAnimationView.playAnimation();






//        Handler mHandler = new Handler();
//        mHandler.postDelayed(new Runnable() {
//
//                                 @Override
//                                 public void run() {
//                                     //startActivity(new Intent(Hist.this, History.class));
//                                    // finish();
//                                     lottieAnimationView.setVisibility(View.GONE);
//                                 }
//
//                             },
//
//                3000L);

        History obj= new History(user_id,context,datamodel2,listView,adapter,lottieAnimationView);
        obj.execute();


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
       Intent i = new Intent(this, ChatActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this, "Success", Toast.LENGTH_LONG).show();

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

}
