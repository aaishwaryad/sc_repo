package com.example.aishu.login;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


import com.Ansh.SmartCart.UnityPlayerActivity;
import com.github.kittinunf.fuel.Fuel;
import com.github.kittinunf.fuel.core.FuelError;
import com.github.kittinunf.fuel.core.Handler;
import com.github.kittinunf.fuel.core.Request;
import com.github.kittinunf.fuel.core.Response;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ibm.watson.developer_cloud.conversation.v1.ConversationService;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageRequest;
import com.ibm.watson.developer_cloud.conversation.v1.model.MessageResponse;
import com.ibm.watson.developer_cloud.http.ServiceCallback;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.aishu.login.RegisterActivity.flag;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private ConversationService myConversationService = null;
    private TextView chatDisplayTV;
    private EditText userStatementET;

   // public static int flag=0;

    private TextView buyList;
    private final String IBM_USERNAME = "****2103-****-****-****-20b9e2******";
    private final String IBM_PASSWORD = "************";
    private final String IBM_WORKSPACE_ID = "****d55b-****-****-****-dde5ef******";

//    private final String IBM_USERNAME = "80002103-9cb1-4c32-90b7-20b9e2cbbcc2";
 //   private final String IBM_PASSWORD = "3Y0qjDB1mKju";
 //   private final String IBM_WORKSPACE_ID = "694a31dd-9ee0-4a45-a9e1-3653daaf81eb";
    Button cartbtn;
    Button signout;
    ImageButton speakBtn;
    Button scan_buy,whereis,addlist, offerbtn, stylist;
    private IntentIntegrator qrScan1;
    String buy;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    KProgressHUD hud;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private NavigationView navigationView;
    Context context;



    TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

//        cartbtn=findViewById(R.id.cart);
//        signout=findViewById(R.id.signout);
        scan_buy=findViewById(R.id.buy_btn);
        whereis=findViewById(R.id.where_is_btn);
        addlist=findViewById(R.id.add_list_btn);
        offerbtn=findViewById(R.id.offer_btn);
        stylist= findViewById(R.id.stylist);

        qrScan1 = new IntentIntegrator(this);
        navigationView=(NavigationView)findViewById(R.id.nav_view);
        View navView=navigationView.inflateHeaderView(R.layout.nav_header);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        buyList=findViewById(R.id.buyList_lay);
        buy=buyList.getText().toString();
        context=getApplicationContext();

      //  buyList.setVisibility(View.INVISIBLE);

        hud =KProgressHUD.create(ChatActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);



        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        if(flag==1)
        {
            SendUsertoProfileActivity();
        }



        speakBtn=(ImageButton)findViewById(R.id.speak_btn);

        speakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                promptSpeechInput();
            }
        });

        addlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  addlist.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                hud.show();
                userStatementET.setText("Add jacket to my wishlist.");
                simicode();
            }
        });

        scan_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // scan_buy.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                hud.show();
                userStatementET.setText("I want to buy the product");
                simicode();
            }
        });

        whereis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  whereis.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                hud.show();
                userStatementET.setText("Where is blazer?");
                simicode();
            }
        });

        offerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //offerbtn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                hud.show();
                userStatementET.setText("What are today's offers?");
                simicode();
            }
        });

        stylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              SendUsertoStylist();
            }
        });

        chatDisplayTV = findViewById(R.id.tv_chat_display);
        userStatementET = (EditText)findViewById(R.id.et_user_statement);

        //instantiating IBM Watson Conversation Service
        myConversationService =
                new ConversationService(
                        "2017-12-06",
                        IBM_USERNAME,
                        IBM_PASSWORD
                );

        userStatementET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView tv, int action, KeyEvent keyEvent) {
                if (action == EditorInfo.IME_ACTION_DONE) {
                    //show the user statement
                    hud.show();
                    simicode();
//                    final String userStatement = userStatementET.getText().toString();
//                    if(userStatement.contains(buy))
//                    {
//                        SendUsertoScanActivity();
//                    }
//                    chatDisplayTV.append(
//                            Html.fromHtml("<p><b>YOU:</b> " + userStatement + "</p>")
//                    );
//                    userStatementET.setText("");
//
//
//
//                    MessageRequest request = new MessageRequest.Builder()
//                            .inputText(userStatement)
//                            .build();
//                    // initiate chat conversation
//                    myConversationService
//                            .message(IBM_WORKSPACE_ID, request)
//                            .enqueue(new ServiceCallback<MessageResponse>() {
//                                @Override
//                                public void onResponse(MessageResponse response) {
//                                    final String botStatement = response.getText().get(0);
//
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                         //   Toast.makeText(getApplicationContext(),botStatement, Toast.LENGTH_LONG).show();
//                                            if(botStatement.contains("buy"))
//                                            {
//                                                SendUsertoScanActivity();
//                                            }
//                                            chatDisplayTV.append(
//                                                    Html.fromHtml("<p><b>BOT:</b> " +
//                                                            botStatement + "</p>")
//                                            );
//
//                                        }
//                                    });
//
//                                    t1.speak(botStatement, TextToSpeech.QUEUE_FLUSH, null);
//
//                                    // if the intent is joke then we access the third party
//                                    // service to get a random joke and respond to user
//                                    if (response.getIntents().get(0).getIntent().endsWith("Joke")) {
//                                        final Map<String, String> params = new HashMap<String, String>() {{
//                                            put("Accept", "text/plain");
//                                        }};
//                                        Fuel.get("https://icanhazdadjoke.com/").header(params)
//                                                .responseString(new Handler<String>() {
//                                                    @Override
//                                                    public void success(Request request, Response response, String body) {
//                                                        Log.d(TAG, "" + response + " ; " + body);
//                                                        chatDisplayTV.append(
//                                                                Html.fromHtml("<p><b>BOT:</b> " +
//                                                                        body + "</p>")
//                                                        );
//                                                    }
//
//                                                    @Override
//                                                    public void failure(Request request, Response response, FuelError fuelError) {
//                                                    }
//                                                });
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(Exception e) {
//                                    Log.d(TAG, e.getMessage());
//                                }
//                            });
                }
                return false;
            }
        });
//        cartbtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(ChatActivity.this,ScanActivity.class));
//            }
//        });
//        signout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FirebaseAuth.getInstance().signOut();
//                startActivity(new Intent(ChatActivity.this,MainActivity.class));
//            }
//        });


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                displaySelectedScreen(id);
                return true;
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatDisplayTV.append(
                        Html.fromHtml("<p><b>BOT:</b> Hello! Welcome to the store. How may I help you? </p>")
                );
            }
        });
        //hud.dismiss();
        t1.speak( "Hello! Welcome to the store. How may I help you?", TextToSpeech.QUEUE_FLUSH, null);
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        if(mToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int id) {
        switch (id) {
            case R.id.nav_cart:
                SendUsertoScanActivity();
                break;
            case R.id.nav_ar:
                sendUsertoArActivity();
                break;
            case R.id.nav_hist:
                SendUsertoHistActivity();
                break;
            case R.id.nav_chatbot:
                SendUsertoChatbotActivity();
                break;
            case R.id.nav_profile:
                SendUsertoProfileActivity();
                break;
            case R.id.nav_signout:
                SendUsertoMainActivity();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private void SendUsertoScanActivity() {
        Intent findIntent=new Intent(ChatActivity.this,ScanActivity.class);
        //findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }

    private void SendUsertoHistActivity() {
        Intent findIntent=new Intent(ChatActivity.this,Hist.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }

    private void SendUsertoChatbotActivity() {
        Intent findIntent=new Intent(ChatActivity.this,ChatActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }
    private void SendUsertoProfileActivity() {
        Intent findIntent=new Intent(ChatActivity.this,RegisterActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }
    private void SendUsertoMainActivity() {
        FirebaseAuth.getInstance().signOut();
        Intent findIntent=new Intent(ChatActivity.this,MainActivity.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);
    }

    private void SendUsertoStylist(){
        Intent findIntent=new Intent(ChatActivity.this,Stylist_webview.class);
        findIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(findIntent);

    }

    private void sendUsertoArActivity() {
        //        Intent findIntent=getPackageManager().getLaunchIntentForPackage("com.Ansh.SmartCart");
//        if(findIntent!=null) {
//            startActivity(findIntent);
//        }
//        Intent intent = new Intent(context, UnityPlayerActivity.class);
//        startActivity(intent);
        Intent findIntent=new Intent(ChatActivity.this,EmptyARActivity.class);
        startActivity(findIntent);
    }

    private void promptSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
//            Toast.makeText(getApplicationContext(),
//                    getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      //  hud.dismiss();

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
              //value= result.getContents();
                //  barcode.setText(value);
               // Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                Bot_Desc bot_desc= new Bot_Desc(result.getContents(),ChatActivity.this);
                bot_desc.execute();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);

            switch (requestCode) {
                case REQ_CODE_SPEECH_INPUT: {
                    if (resultCode == RESULT_OK && null != data) {
                      //  hud.dismiss();
                        ArrayList<String> result1 = data
                                .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        userStatementET.setText(result1.get(0));
                        simicode();
//                    final String userStatement = userStatementET.getText().toString();
//
//
//                        chatDisplayTV.append(
//                                Html.fromHtml("<p><b>YOU:</b> " + userStatement + "</p>")
//                        );
//                        userStatementET.setText("");
//
//                    MessageRequest request = new MessageRequest.Builder()
//                            .inputText(userStatement)
//                            .build();
//                    // initiate chat conversation
//                    myConversationService
//                            .message(IBM_WORKSPACE_ID, request)
//                            .enqueue(new ServiceCallback<MessageResponse>() {
//                                @Override
//                                public void onResponse(MessageResponse response) {
//                                    final String botStatement = response.getText().get(0);
//                                    if(botStatement.contains("buy"))
//                                    {
//                                        SendUsertoScanActivity();
//                                    }
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            chatDisplayTV.append(
//                                                    Html.fromHtml("<p><b>BOT:</b> " +
//                                                            botStatement + "</p>")
//                                            );
//                                        }
//                                    });
//
//                                 //   t1.speak(botStatement, TextToSpeech.QUEUE_FLUSH, null);
//
//                                    // if the intent is joke then we access the third party
//                                    // service to get a random joke and respond to user
//                                    if (response.getIntents().get(0).getIntent().endsWith("Joke")) {
//                                        final Map<String, String> params = new HashMap<String, String>() {{
//                                            put("Accept", "text/plain");
//                                        }};
//                                        Fuel.get("https://icanhazdadjoke.com/").header(params)
//                                                .responseString(new Handler<String>() {
//                                                    @Override
//                                                    public void success(Request request, Response response, String body) {
//                                                        Log.d(TAG, "" + response + " ; " + body);
//                                                        chatDisplayTV.append(
//                                                                Html.fromHtml("<p><b>BOT:</b> " +
//                                                                        body + "</p>")
//                                                        );
//                                                    }
//
//                                                    @Override
//                                                    public void failure(Request request, Response response, FuelError fuelError) {
//                                                    }
//                                                });
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(Exception e) {
//                                    Log.d(TAG, e.getMessage());
//                                }
//                            });
                    }
                    break;
                }

            }
        }
    }

    void simicode()
    {
        final String userStatement = userStatementET.getText().toString();
        int i=0;
//hud.dismiss();
        chatDisplayTV.append(
                Html.fromHtml("<p><b>YOU:</b> " + userStatement + "</p>")
        );
        userStatementET.setText("");

        MessageRequest request = new MessageRequest.Builder()
                .inputText(userStatement)
                .build();
        // initiate chat conversation
        myConversationService
                .message(IBM_WORKSPACE_ID, request)
                .enqueue(new ServiceCallback<MessageResponse>() {
                    @Override
                    public void onResponse(MessageResponse response) {
                        final String botStatement = response.getText().get(0);
                      //  Toast.makeText(ChatActivity.this,response.getText().toString(),Toast.LENGTH_SHORT).show();
                        hud.dismiss();

                        if(botStatement.contains("??navigate"))
                        {
                            sendUsertoArActivity();
                        }
                        if(botStatement.contains("??buy"))
                        {
                            SendUsertoScanActivity();
                        }
                        else if(botStatement.contains("??desc"))
                        {
                            qrScan1.initiateScan();
                        }
                        else if(botStatement.contains("??add"))
                        {
                            buy=buy+botStatement.substring(5)+",";
                            //buyList.setVisibility(View.VISIBLE);
                            buyList.setText(buy);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    chatDisplayTV.append(
                                            Html.fromHtml("<p><b>BOT:</b> Added " +
                                                    botStatement.substring(5) + " to the list.</p>")
                                    );
                                }
                            });
                            //hud.dismiss();
                            t1.speak( "Added " + botStatement.substring(5) + " to the list", TextToSpeech.QUEUE_FLUSH, null);

                          //  buyList.setVisibility(View.VISIBLE);

                        }
//                        else if(botStatement.contains("??show"))
//                        {
//                            buyList.setVisibility(View.VISIBLE);
//                        }
                        else if(botStatement.contains("??where"))
                        {
                            WhereisDB obj=new WhereisDB(botStatement.substring(8),ChatActivity.this);
                            obj.execute();

                        }
                        else{
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    chatDisplayTV.append(
                                            Html.fromHtml("<p><b>BOT:</b> " +
                                                    botStatement + "</p>")
                                    );
                                }
                            });
                            //hud.dismiss();
                                t1.speak(botStatement, TextToSpeech.QUEUE_FLUSH, null);


                            // if the intent is joke then we access the third party
                            // service to get a random joke and respond to user
                            if (response.getIntents().get(0).getIntent().endsWith("Joke")) {
                                final Map<String, String> params = new HashMap<String, String>() {{
                                    put("Accept", "text/plain");
                                }};
                                Fuel.get("https://icanhazdadjoke.com/").header(params)
                                        .responseString(new Handler<String>() {
                                            @Override
                                            public void success(Request request, Response response, String body) {
                                                Log.d(TAG, "" + response + " ; " + body);
                                                chatDisplayTV.append(
                                                        Html.fromHtml("<p><b>BOT:</b> " +
                                                                body + "</p>")
                                                );
                                            }

                                            @Override
                                            public void failure(Request request, Response response, FuelError fuelError) {
                                            }
                                        });
                            }
                        }

                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.d(TAG, e.getMessage());
                    }
                });



    }


    public void loc_print(final String loc)
    {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                chatDisplayTV.append(
                        Html.fromHtml("<p><b>BOT:</b> Location of the item is " +
                                loc + ".  Can I help you with anything else?</p>")
                );
                t1.speak( "Location of the item is " + loc + ". Can I help you with anything else?", TextToSpeech.QUEUE_FLUSH, null);
            }
        });
    }

}
