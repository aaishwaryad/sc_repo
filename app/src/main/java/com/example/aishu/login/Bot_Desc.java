package com.example.aishu.login;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Bot_Desc extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/description.php";
    String tid;
    Context context;

 //   Dialog dialog;
    TextToSpeech t1;


    public Bot_Desc(String tid, Context context) {
        this.tid=tid;
        this.context=context;
     //   this.dialog=dialog;
        t1=new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

    }


    @Override
    protected Object doInBackground(Object[] objects) {
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                {
                    // Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                    try {
                        //    String jsonStr = "[{\"prod_id\":,\"prod_name\":\"prod_cat\",\"prod_price\":,\"prod_imgurl\":,\"prod_time\":}]";
                        //    JSONArray jsonarray = new JSONArray(jsonStr);
                        JSONObject c = new JSONObject(response);
                        String prod_id = c.getString("product_id");
                        String prod_name = c.getString("product_name");
                        String prod_price = c.getString("product_price");
                        String prod_imgurl = c.getString("img_url");
                        String prod_cat = c.getString("product_category");
                        String prod_brand=c.getString("product_brand");
                        String prod_des=c.getString("description");
                        //   Toast.makeText(context,prod_name,Toast.LENGTH_SHORT).show();
                        // dataModel3.add(new DataModel3(prod_id, prod_name, prod_price, prod_imgurl, prod_time, prod_des));
                        //   View_desc_dialog view_desc_dialog=new View_desc_dialog();
                      //  ((ScanActivity)context).showDialog(prod_id,prod_name,prod_price,prod_imgurl, prod_cat, prod_brand, prod_des);

                        // dialog=new Dialog(context);

                        t1.speak(prod_des, TextToSpeech.QUEUE_FLUSH, null);
                        Toast.makeText(context,prod_des,Toast.LENGTH_LONG).show();

//                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                        dialog.setCancelable(false);
//                        dialog.setContentView(R.layout.activity_desc);
//
//
//                        TextView iname= (TextView) dialog.findViewById(R.id.iname);
//                        TextView iprice= (TextView) dialog.findViewById(R.id.iprice);
//                        ImageView imgurl= (ImageView) dialog.findViewById(R.id.img);
//                        TextView icat= (TextView) dialog.findViewById(R.id.icat);
//                        TextView ibrand= (TextView) dialog.findViewById(R.id.ibrand);
//                        TextView idesc= (TextView) dialog.findViewById(R.id.desc);
//                        ImageButton close=(ImageButton) dialog.findViewById(R.id.close);
//
//                        Toast.makeText(context, prod_des,Toast.LENGTH_SHORT).show();
//
//                        iname.setText(prod_name);
//                        iprice.setText(prod_price);
//                        icat.setText(prod_cat);
//                        ibrand.setText(prod_brand);
//                        idesc.setText(prod_des);
//                        Picasso.with(context).load(prod_imgurl).into(imgurl);
//
//                        close.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                dialog.dismiss();
//                            }
//                        });
//                       // dialog.getWindow();
//                        ((ScanActivity)context).dialog.show();




                    } catch (JSONException e) {
                        Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",tid);
                return params;
            }
        };
        mRequestQueue.add(postrequest);
        return null;
    }
}
