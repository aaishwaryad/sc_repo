package com.example.aishu.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterNewUser extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    Button register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_user);
        firebaseAuth = FirebaseAuth.getInstance();
        register=findViewById(R.id.register);



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // register.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
                String email = ((EditText)findViewById(R.id.emailET)).getText().toString();
                String password = ((EditText)findViewById(R.id.passwordET)).getText().toString();
                if(email.length()>0 && password.length()>0)
                {
                    firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegisterNewUser.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful())
                            {
                                startActivity(new Intent(RegisterNewUser.this, RegisterActivity.class));

                            } else
                            {
                                Toast.makeText(RegisterNewUser.this, "Incorrect Format", Toast.LENGTH_LONG).show();
                            }

                        }
                    });
                }
                else
                {
                    Toast.makeText(RegisterNewUser.this, "Data is missing", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
