package com.example.aishu.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Retrieve_profile extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/profile.php";
    //String fname="", lname="", phone="", age1="",gender_value="", email=" ";
    EditText fname,lname,phone,age1,balance;
    RadioGroup gender;
    String email;
    Context context;
    KProgressHUD hud;
    RadioButton btn,fbtn,mbtn;



    public Retrieve_profile(EditText fname, EditText lname, EditText phone, EditText age1, EditText balance, RadioGroup gender, Context context, String email, KProgressHUD hud,RadioButton fbtn, RadioButton mbtn)
    {
        this.fname=fname;
        this.lname=lname;
        this.phone=phone;
        this.age1=age1;
        this.balance=balance;
        this.gender=gender;
        this.email=email;
        this.context=context;
        this.hud= hud;
        this.fbtn=fbtn;
        this.mbtn=mbtn;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(context,response,Toast.LENGTH_SHORT).show();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject c = jsonArray.getJSONObject(0);
                    String fname1 = c.getString("FNAME");
                    String lname1 = c.getString("LNAME");
                    String gender1 = c.getString("GENDER");
                    String age11 = c.getString("AGE");
                    String balance1 = c.getString("BALANCE");
                    String phone1=c.getString("PHONE");
                    hud.dismiss();;
                    //Toast.makeText(context,gender1,Toast.LENGTH_SHORT).show();

                    if(gender1.equalsIgnoreCase("female"))
                    {
                        fbtn.setChecked(true);
                    }
                    if(gender1.equalsIgnoreCase("male"))
                    {
                        mbtn.setChecked(true);
                    }

                    fname.setText(fname1);
                    lname.setText(lname1);
                    phone.setText(phone1);
                    balance.setText(balance1);
                    age1.setText(age11);

                } catch (JSONException e) {
                    e.printStackTrace();
                    hud.dismiss();
                   // Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                    fname.setEnabled(true);
                    lname.setEnabled(true);
                    phone.setEnabled(true);
                    age1.setEnabled(true);
                    balance.setEnabled(true);
                    fbtn.setEnabled(true);
                    mbtn.setEnabled(true);
                    gender.setEnabled(true);
                    RegisterActivity.flag=1;
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                        fname.setEnabled(true);
                        lname.setEnabled(true);
                        phone.setEnabled(true);
                        age1.setEnabled(true);
                        fbtn.setEnabled(true);
                        mbtn.setEnabled(true);
                        balance.setEnabled(true);
                        gender.setEnabled(true);
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("user_id",email);

                return params;
            }
        };
        mRequestQueue.add(postrequest);

        return null;
    }

    private void sendUserToChatActivity() {
        Intent mainIntent=new Intent(context,ChatActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(mainIntent);
        //((Activity)context).finish();
        // context.do
    }

}

