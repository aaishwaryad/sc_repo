package com.example.aishu.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel>  {
    private ArrayList<DataModel> DataSet;
    Context mcontext;

    private static class ViewHolder{
        TextView iname, cat, price;
        ImageView imgurl;
    }

    public CustomAdapter(ArrayList<DataModel>data, Context context){
        super(context, R.layout.item,data);
        this.DataSet=data;
        this.mcontext=context;

    }


    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        DataModel datamodel=getItem(position);
        ViewHolder viewholder;
        final View result;
        if(convertView == null)
        {
            viewholder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item, parent, false);
            viewholder.iname = (TextView) convertView.findViewById(R.id.iname);
            viewholder.cat = (TextView) convertView.findViewById(R.id.icat);
            viewholder.price = (TextView) convertView.findViewById(R.id.iprice);
            viewholder.imgurl = (ImageView) convertView.findViewById(R.id.img);
            result=convertView;

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        viewholder.iname.setText(datamodel.getIname());
        viewholder.cat.setText(datamodel.getCat());
        viewholder.price.setText(datamodel.getPrice());
        Picasso.with(mcontext).load(datamodel.getImgurl()).into(viewholder.imgurl);


       // viewholder.imgurl.setOnClickListener(this);
      //  viewholder.imgurl.setTag(position);

        return convertView;
    }
}
