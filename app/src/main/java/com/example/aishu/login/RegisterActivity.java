package com.example.aishu.login;

import android.app.AppComponentFactory;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.common.util.ProcessUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.kaopiz.kprogresshud.KProgressHUD;

public class RegisterActivity extends AppCompatActivity {
    EditText firstname, lastname, phn, age,balance;
    RadioGroup gender;
    int selectedgender;
    Button submit_btn;
    RadioButton btn,fbtn,mbtn;
    Context context;
    String email;
    FirebaseAuth mAuth;
    KProgressHUD hud;
    ImageButton editbtn;
    String gender_value;
    public static int flag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Profile");
        context=getApplicationContext();
        mAuth = FirebaseAuth.getInstance();
        firstname = (EditText) findViewById(R.id.firstname);
        lastname = ((EditText) findViewById(R.id.lastname));
        phn = (EditText) findViewById(R.id.phn);
        age = (EditText) findViewById(R.id.age);
        balance = (EditText) findViewById(R.id.etbal);
        gender = (RadioGroup) findViewById(R.id.gender_radio);
        submit_btn=(Button) findViewById(R.id.submit_btn);
        editbtn=(ImageButton) findViewById(R.id.editbtn);
        fbtn=(RadioButton) findViewById(R.id.female);
        mbtn=(RadioButton) findViewById(R.id.male);

        firstname.setEnabled(false);
        lastname.setEnabled(false);
        phn.setEnabled(false);
        age.setEnabled(false);
        fbtn.setEnabled(false);
        mbtn.setEnabled(false);
        gender.setEnabled(false);
        balance.setEnabled(false);

        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        email =firebaseUser.getEmail();

        hud =KProgressHUD.create(RegisterActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f).show();

        Retrieve_profile retrieve_profile= new Retrieve_profile(firstname,lastname,phn,age,balance,gender,context,email,hud,fbtn,mbtn);
        retrieve_profile.execute();

    editbtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            firstname.setEnabled(true);
            lastname.setEnabled(true);
            phn.setEnabled(true);
            age.setEnabled(true);
            fbtn.setEnabled(true);
            mbtn.setEnabled(true);
            gender.setEnabled(true);
            balance.setEnabled(true);
        }
    });


    submit_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           // submit_btn.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_pressed));
            String fname = firstname.getText().toString();
            String lname = lastname.getText().toString();
            String phone = phn.getText().toString();
            String age1 = age.getText().toString();
            String bal=balance.getText().toString();
            selectedgender=gender.getCheckedRadioButtonId();
            btn=findViewById(selectedgender);
            try{
                gender_value=btn.getTag().toString();
            }catch (Exception e)
            {

            }

            RegisterDB obj= new RegisterDB(fname, lname, phone, age1, bal, gender_value,email, context);
            obj.execute();

        }
    });

}
    @Override
    public void onBackPressed() {

        super.onBackPressed();
        Intent i = new Intent(this, ChatActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
}

