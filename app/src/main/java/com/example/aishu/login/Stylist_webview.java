package com.example.aishu.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class Stylist_webview extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stylist);
        setTitle("SmartCart Stylist");

        webView = (WebView)findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.loadUrl("https://assistant-chat-eu-gb.watsonplatform.net/web/public/a1e276a5-b5c7-481b-b2d8-3d50a1bb580b");


//        StringBuilder sb = new StringBuilder();
//        sb.append("<HTML><HEAD><LINK href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"/></HEAD><body>");
//      //  sb.append(tables.toString());
//        sb.append("</body></HTML>");
//        webView.loadDataWithBaseURL("https://assistant-chat-eu-gb.watsonplatform.net/web/public/a1e276a5-b5c7-481b-b2d8-3d50a1bb580b", sb.toString(), "text/html", "utf-8", null);

    }
}
