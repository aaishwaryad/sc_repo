package com.example.aishu.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter2 extends ArrayAdapter<DataModel2>  {
    private ArrayList<DataModel2> DataSet;
    Context mcontext;

    private static class ViewHolder{
        TextView iname,price, itime;
        ImageView imgurl;
    }

    public CustomAdapter2(ArrayList<DataModel2>data, Context context){
        super(context, R.layout.hist,data);
        this.DataSet=data;
        this.mcontext=context;

    }


    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        DataModel2 datamodel=getItem(position);
        ViewHolder viewholder;
        final View result;
        if(convertView == null)
        {
            viewholder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.hist, parent, false);
            viewholder.iname = (TextView) convertView.findViewById(R.id.iname);
            viewholder.price = (TextView) convertView.findViewById(R.id.iprice);
            viewholder.itime = (TextView) convertView.findViewById(R.id.itime);
            viewholder.imgurl = (ImageView) convertView.findViewById(R.id.img);
            result=convertView;

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        try{
            viewholder.iname.setText(datamodel.getIname());
            viewholder.price.setText(datamodel.getPrice());
            viewholder.itime.setText(datamodel.getItime());
            Picasso.with(mcontext).load(datamodel.getImgurl()).into(viewholder.imgurl);

        }catch (Exception e)
        {

        }


        // viewholder.imgurl.setOnClickListener(this);
        //  viewholder.imgurl.setTag(position);

        return convertView;
    }
}
