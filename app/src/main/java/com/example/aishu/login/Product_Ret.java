package com.example.aishu.login;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Product_Ret extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/qr.php";
    String value="";
    Context context;
    ArrayList<DataModel> dataModel;
    ListView listView;
    CustomAdapter adapter;
    String user_id;
    TextView txtprice;
    String price;
    int pprice;
    KProgressHUD hud;


    public Product_Ret(String user_id, String value, Context context, ArrayList<DataModel> dataModel, ListView listView, CustomAdapter adapter, TextView txtprice, KProgressHUD hud) {
        this.user_id=user_id;
        this.value=value;
        this.context=context;
        this.dataModel=dataModel;
        this.listView=listView;
        this.adapter=adapter;
        this.txtprice=txtprice;
        this.hud=hud;
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                  // Toast.makeText(context,response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject reader= new JSONObject(response);
                    String prod_id=reader.getString("product_id");
                    String prod_name=reader.getString("product_name");
                    String prod_cat=reader.getString("product_category");
                    String prod_price=reader.getString("product_price");
                    String prod_imgurl=reader.getString("img_url");
                    dataModel.add(new DataModel(prod_id, prod_name, prod_price, prod_imgurl,prod_cat));
                    adapter= new CustomAdapter(dataModel,context);
                    listView.setAdapter(adapter);
                    price=txtprice.getText().toString();
                    pprice=Integer.parseInt(price)+Integer.parseInt(prod_price);
                    txtprice.setText(pprice+"");
                    hud.dismiss();

                   // Toast.makeText(context,Integer.parseInt(prod_price),Toast.LENGTH_SHORT).show();


                 //   String prod_imgurl=reader.getString("product_imgurl");
//                    JSONObject product_name  = reader.getJSONObject("product_name");
//
//                   JSONObject product_price  = reader.getJSONObject("product_price");
//                    JSONObject product_cat  = reader.getJSONObject("product_cat");
//                    JSONObject product_imgurl  = reader.getJSONObject("product_imgurl");
                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                        hud.dismiss();

                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("product_id",value);
                params.put("user_id",user_id);
                return params;
            }
        };
        mRequestQueue.add(postrequest);
        return null;
    }
}
