package com.example.aishu.login;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class History extends AsyncTask {
    private static final String baseurl="https://smartcart2.000webhostapp.com/history.php";
    Context context;
    ArrayList<DataModel2> dataModel2;
    ListView listView;
    CustomAdapter2 adapter;
    String user_id;
    LottieAnimationView lottieAnimationView;
//KProgressHUD hud;


    public History(String user_id, Context context, ArrayList<DataModel2> dataModel2, ListView listView, CustomAdapter2 adapter,LottieAnimationView lottieAnimationView) {
        this.user_id=user_id;
        this.context=context;
        this.dataModel2=dataModel2;
        this.listView=listView;
        this.adapter=adapter;
     //  this.hud=hud;
        this.lottieAnimationView=lottieAnimationView;

    }

    @Override
    protected Object doInBackground(Object[] objects) {

        RequestQueue mRequestQueue;
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024);
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();




        StringRequest postrequest= new StringRequest(Request.Method.POST, baseurl, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                {
                 //   Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                    try {
                    //    String jsonStr = "[{\"prod_id\":,\"prod_name\":\"prod_cat\",\"prod_price\":,\"prod_imgurl\":,\"prod_time\":}]";
                    //    JSONArray jsonarray = new JSONArray(jsonStr);
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject c = jsonArray.getJSONObject(i);
                            String prod_id = c.getString("product_id");
                            String prod_name = c.getString("product_name");
                            String prod_price = c.getString("price");
                            String prod_imgurl = c.getString("img_url");
                            String prod_time = c.getString("time");
                            //Toast.makeText(context,prod_name,Toast.LENGTH_SHORT).show();
                            dataModel2.add(new DataModel2(prod_id, prod_name, prod_price, prod_imgurl, prod_time));
                            adapter= new CustomAdapter2(dataModel2,context);
                            listView.setAdapter(adapter);
                           // hud.dismiss();
                            lottieAnimationView.setVisibility(View.GONE);

                        }



                    } catch (JSONException e) {
                        Toast.makeText(context,e.toString(),Toast.LENGTH_SHORT).show();
                      //  hud.dismiss();
                        lottieAnimationView.setVisibility(View.GONE);

                        e.printStackTrace();
                    }
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params=new HashMap<>();
                params.put("user_id",user_id);
                return params;
            }
        };
        mRequestQueue.add(postrequest);
        return null;
    }

//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        hud =KProgressHUD.create(context)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//                .setLabel("Please wait")
//                .setDetailsLabel("Downloading data")
//                .setCancellable(true)
//                .setAnimationSpeed(2)
//                .setDimAmount(0.5f)
//                .show();
//
//    }

}
